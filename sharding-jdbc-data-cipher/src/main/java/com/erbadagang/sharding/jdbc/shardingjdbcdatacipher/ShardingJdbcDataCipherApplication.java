package com.erbadagang.sharding.jdbc.shardingjdbcdatacipher;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 项目启动类
 * @ClassName: ShardingJdbcDataCipherApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/2 16:12
 * @Copyright:
 */
@SpringBootApplication
@MapperScan(value = "com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.dao")
public class ShardingJdbcDataCipherApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcDataCipherApplication.class, args);
    }

}
