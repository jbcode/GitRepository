package com.vue.backend.api;

import com.vue.backend.api.config.ApiVersion;
import com.vue.backend.entity.ProgramLanguage;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName: ProgramLanguageController
 * @Description:为vue的小程序及h5页面提供数据接口。
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020年4月3日 下午9:29:05
 * @Copyright:
 */
@RestController
//The list of allowed origins that be specific origins, e.g. "https://domain1.com", or "*" for all origins.
@CrossOrigin(origins = "*") // 允许h5跨域访问。访问的接口IP地址/域名或者端口号与h5页面不同就涉及跨域。
@RequestMapping(value = "/{version}/vue/api") // version标识在url的最前端。http://localhost:8888/v2/
public class ProgramLanguageController {

    // 初始化数据，供接口调用返回给Vue前端展示。
    private List<String> languageList = new ArrayList<String>();
    private Map<String, ProgramLanguage> allLanguageInfos = new HashMap<String, ProgramLanguage>();

    public ProgramLanguageController() {
        // 初始化首页列表数据
        languageList.add("C");
        languageList.add("vue");
        languageList.add("java");
        languageList.add("PHP");
        languageList.add("Python");
        languageList.add("C++");

        // 初始化详情页数据
        allLanguageInfos.put("vue", new ProgramLanguage("vue.js", 5, "前端语言框架门槛较低"));
        allLanguageInfos.put("java", new ProgramLanguage("Java", 8, "Java是Android操作系统的基石。大多数大型系统基于Java"));
        allLanguageInfos.put("go", new ProgramLanguage("golang", 6, "越来越流行"));
        allLanguageInfos.put("Python",
                new ProgramLanguage("Python", 6, "Python是一项服务器端解释型开源非编译脚本语言。可单独使用，也可作为django等框架的组成部分"));
        allLanguageInfos.put("PHP", new ProgramLanguage("PHP", 7, "PHP是一款服务器端脚本语言，主要面向Web开发但同时作为通用性编程语言"));
        allLanguageInfos.put("C", new ProgramLanguage("C", 10,
                "万物之源C语言。C语言于1969年至1973年之间由AT&T公司旗下贝尔实验室创建完成，用于构建Unix操作系统。C语言是一种通用型命令式计算机编程语言，其支持结构化编程、词汇变量范围与递归，同时亦是套能够预防各类未预期操作的静态类型系统。 其最初构建目标在于编写系统软件。"));
        allLanguageInfos.put("C++", new ProgramLanguage("C++", 10, "面向对象"));
    }

    /**
     * 在首页的列表中展示的所有语言信息。
     *
     * @return
     */
    @ApiVersion(2) // 加入接口url的版本控制，http://localhost:8888/v2/vue/api/programLanguage/getAll
    @RequestMapping(value = "/programLanguage/getAll")
    public List<String> getAllSimpleLanguageName() {
        return languageList;
    }

    /**
     * 在首页的列表中展示的所有语言信息。
     *
     * @return
     */
    @ApiVersion(2) // 加入接口url的版本控制，http://localhost:8888/v2/vue/api/programLanguage/getByName?language_name=C
    @RequestMapping(value = "/programLanguage/getByName")
    public List<String> getByName(@RequestParam String language_name) {
        List<String> filterList = languageList.stream().filter(s -> s.toLowerCase().contains(language_name.toLowerCase())).collect(Collectors.toList());
      /*  List<String> filterList = new ArrayList<String>();
        languageList.forEach(c -> {
            if (c.toLowerCase().contains(language_name.toLowerCase()))
                filterList.add(c);
        });*/
        return filterList;
    }

    /**
     * 在详情页面中展示的具体某个语言信息。
     *
     * @return
     */
    @ApiVersion(2) // 加入接口url的版本控制，http://localhost:8888/v2/vue/api/programLanguage/getdetail/vue
    @RequestMapping(value = "/programLanguage/getdetail/{language_name}")
    public ProgramLanguage getLanguageInfo(@PathVariable String language_name) {
        return allLanguageInfos.get(language_name);
    }

}
