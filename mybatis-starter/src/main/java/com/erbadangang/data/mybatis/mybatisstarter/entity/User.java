package com.erbadangang.data.mybatis.mybatisstarter.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 数据库实体类
 * @ClassName: User
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 10:04
 * @Copyright:
 */
@Data
@NoArgsConstructor
public class User {

    private Long id;

    private String name;
    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}