package com.erbadangang.data.mybatis.mybatisstarter.service;

import com.erbadangang.data.mybatis.mybatisstarter.entity.User;

import java.util.List;

/**
 * @description User服务接口
 * @ClassName: UserService
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 10:22
 * @Copyright:
 */
public interface UserService {

    List<User> findByName(String name);

    int insert(String name, Integer age);

}