package com.erbadagang.dataway;

import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @description Dataway集成在Springboot项目中，通过UI方式配置接口。
 * @ClassName: DatawayApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/6/25 16:39
 * @Copyright:
 */
@SpringBootApplication
@EnableHasor()      // 在Spring 中启用 Hasor
@EnableHasorWeb()   // 将 hasor-web 配置到 Spring 环境中，Dataway 的 UI 是通过 hasor-web 提供服务。
public class DatawayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatawayApplication.class, args);
    }

}
