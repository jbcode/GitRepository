package com.erbadagang.mybatis.plus.activerecord.service;

import com.erbadagang.mybatis.plus.activerecord.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-11
 */
public interface IUserService extends IService<User> {

}
