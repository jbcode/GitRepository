package com.erbadagang.mybatis.plus.activerecord.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.erbadagang.mybatis.plus.activerecord.entity.User;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-11
 */
//@Mapper //启动类增加注解“@MapperScan("com.erbadagang.mybatis.plus.activerecord.mapper")”，所有的mapper接口都不用@Mapper
public interface UserMapper extends BaseMapper<User> {

}
