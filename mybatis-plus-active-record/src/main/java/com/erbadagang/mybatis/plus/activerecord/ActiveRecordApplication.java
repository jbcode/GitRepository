package com.erbadagang.mybatis.plus.activerecord;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @description Active Record围绕数据对象进行CRUD操作。
 * @ClassName: ActiveRecordApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/12 19:12
 * @Copyright:
 */
@SpringBootApplication
@MapperScan("com.erbadagang.mybatis.plus.activerecord.mapper")
public class ActiveRecordApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActiveRecordApplication.class, args);
    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
