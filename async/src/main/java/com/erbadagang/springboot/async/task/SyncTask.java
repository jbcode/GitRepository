package com.erbadagang.springboot.async.task;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @description 同步调用模拟类，定义3个方法，输出执行信息及时长。后续在junit test中顺序调用。
 * @ClassName: SyncTask
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/24 10:28
 * @Copyright:
 */
@Component
public class SyncTask {

    public static Random random = new Random();

    public void doTaskOne() throws Exception {
        System.out.println("开始做任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        System.out.println("完成任务一，耗时：" + (end - start) + "毫秒");
    }

    public void doTaskTwo() throws Exception {
        System.out.println("开始做任务二");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        System.out.println("完成任务二，耗时：" + (end - start) + "毫秒");
    }

    public void doTaskThree() throws Exception {
        System.out.println("开始做任务三");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(5000));
        long end = System.currentTimeMillis();
        System.out.println("完成任务三，耗时：" + (end - start) + "毫秒");
    }

}