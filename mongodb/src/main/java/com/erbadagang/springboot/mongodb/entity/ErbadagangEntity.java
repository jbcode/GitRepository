package com.erbadagang.springboot.mongodb.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @description 实体类对应MongoDB的Document即对应表。
 * @ClassName: ErbadagangEntity
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/8/8 18:05
 * @Copyright:
 */
@Data
@Document(collection = "erbadagang_db")
public class ErbadagangEntity implements Serializable {

    @Id
    private Long id;

    private String title;

    private String description;

    private String by;

    private String url;

}
