package com.erbadagang.springboot.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 启动类。
 * @ClassName: MongodbApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/8/8 17:09
 * @Copyright:
 */
@SpringBootApplication
public class MongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongodbApplication.class, args);
    }

}
