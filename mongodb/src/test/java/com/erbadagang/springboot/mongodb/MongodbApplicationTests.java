package com.erbadagang.springboot.mongodb;

import com.erbadagang.springboot.mongodb.dao.IErbadagangDao;
import com.erbadagang.springboot.mongodb.entity.ErbadagangEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @description 测试类。
 * @ClassName: MongodbApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/8/8 18:08
 * @Copyright:
 */
@SpringBootTest
class MongodbApplicationTests {

    @Autowired
    private IErbadagangDao demoDao;

    @Test
    public void saveDemoTest() {
        ErbadagangEntity erbadagangEntity = new ErbadagangEntity();
        erbadagangEntity.setId(1L);
        erbadagangEntity.setTitle("Spring Boot 中使用 MongoDB");
        erbadagangEntity.setDescription("关注公众号，搜云库，专注于开发技术的研究与知识分享");
        erbadagangEntity.setBy("souyunku");
        erbadagangEntity.setUrl("http://www.souyunku.com");

        demoDao.saveDemo(erbadagangEntity);

        erbadagangEntity = new ErbadagangEntity();
        erbadagangEntity.setId(2L);
        erbadagangEntity.setTitle("Spring Boot 中使用 MongoDB");
        erbadagangEntity.setDescription("关注公众号，搜云库，专注于开发技术的研究与知识分享");
        erbadagangEntity.setBy("souyunku");
        erbadagangEntity.setUrl("http://www.souyunku.com");

        demoDao.saveDemo(erbadagangEntity);
    }

    @Test
    public void removeDemoTest() {
        demoDao.removeDemo(2L);
    }

    @Test
    public void updateDemoTest() {

        ErbadagangEntity erbadagangEntity = new ErbadagangEntity();
        erbadagangEntity.setId(1L);
        erbadagangEntity.setTitle("Spring Boot 中使用 MongoDB 更新数据");
        erbadagangEntity.setDescription("关注公众号，搜云库，专注于开发技术的研究与知识分享");
        erbadagangEntity.setBy("souyunku");
        erbadagangEntity.setUrl("http://www.souyunku.com");

        demoDao.updateDemo(erbadagangEntity);
    }

    @Test
    public void findDemoByIdTest() {
        ErbadagangEntity erbadagangEntity = demoDao.findDemoById(1L);
        System.out.println(erbadagangEntity);
    }
}
