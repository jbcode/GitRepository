package com.erbadagang.mybatis.plus.mybatisplus.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @description 自定义的数据填充handler，分别写insert和update的写入策略。
 * @ClassName: MyFillDataMetaObjectHandler
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/11 9:39
 * @Copyright:
 */
@Component
public class MyFillDataMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "operator", String.class, "梅西爱骑车");
        //逻辑删除
        this.strictInsertFill(metaObject, "deleteFlag", Integer.class, 0);
        //乐观锁version初始化值为1
        this.strictInsertFill(metaObject, "version", Integer.class, 1);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "operator", String.class, "梅西爱骑车");
    }
}
