package com.erbadagang.mybatis.plus.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.erbadagang.mybatis.plus.mybatisplus.entity.User;
import com.erbadagang.mybatis.plus.mybatisplus.mapper.UserMapper;
import com.erbadagang.mybatis.plus.mybatisplus.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
