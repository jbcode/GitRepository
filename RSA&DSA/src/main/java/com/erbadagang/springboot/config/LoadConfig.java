package com.erbadagang.springboot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * LoadConfig作用是：主要用于配置文件中的指定键值对映射到一个java实体类上。
 *
 * @ClassName: LoadConfig
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/30 9:22
 * @Copyright:
 */
// @ConfigurationProperties方式
@Configuration
@ConfigurationProperties(prefix = "guo")
public class LoadConfig {
    // @Value方式
//    @Value("${guo.name}")
//    private String name;


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
