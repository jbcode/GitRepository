package com.erbadagang.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsaDsaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RsaDsaApplication.class, args);
    }
}
