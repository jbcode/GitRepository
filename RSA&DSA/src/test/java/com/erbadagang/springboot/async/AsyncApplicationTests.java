package com.erbadagang.springboot.async;

import com.erbadagang.springboot.config.LoadConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AsyncApplicationTests {


    @Autowired
    private LoadConfig loadConfig;

    @Test
    void testConfig() {
        System.out.println("loadConfig.getName() = " + loadConfig.getName());
    }

    // @Value方式
    @Value("${guo.name}")
    private String configname;

    @Test
    void testConfigValue() {
        System.out.println("configname = " + configname);
    }
}
