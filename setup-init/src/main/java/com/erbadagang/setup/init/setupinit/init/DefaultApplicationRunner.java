package com.erbadagang.setup.init.setupinit.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @description 应用启动时读取传入参数
 * @ClassName: DefaultApplicationRunner
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/6/26 19:27
 * @Copyright:
 */
@Slf4j
@Component
public class DefaultApplicationRunner implements ApplicationRunner, Ordered {

    @Autowired
    private SetupInit.InitMethodBean initMethodBean;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("i am applicationRunner! invoke the bean method:" + initMethodBean.getServiceName());

        args.getOptionNames().forEach(System.out::println);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>");
        String[] sourceArgs = args.getSourceArgs();

        if (sourceArgs != null) {
            for (String sourceArg : sourceArgs) {
                System.out.println("sourceArg = " + sourceArg);
            }
        }


        log.info("<<<<<<<<<<<KV对参数<<<<<<<<<<<<<<");
        List<String> foo = args.getOptionValues("dev.name");
        if (!CollectionUtils.isEmpty(foo)) {
            foo.forEach(System.out::println);
        }

        log.info("++++++单身狗参数++++++");
        List<String> nonOptionArgs = args.getNonOptionArgs();
        log.info("nonOptionArgs.size() = " + nonOptionArgs.size());
        nonOptionArgs.forEach(System.out::println);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 65;
    }
}
