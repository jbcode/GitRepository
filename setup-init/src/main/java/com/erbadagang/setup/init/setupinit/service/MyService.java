package com.erbadagang.setup.init.setupinit.service;

import org.springframework.stereotype.Service;

/**
 * MyService作用是：模拟一个service被springboot初始化代码调用。
 *
 * @ClassName: MyService
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/6/26 16:42
 * @Copyright:
 */
@Service
public class MyService {
    public String getName() {
        return "guoxiuzhi";
    }
}
