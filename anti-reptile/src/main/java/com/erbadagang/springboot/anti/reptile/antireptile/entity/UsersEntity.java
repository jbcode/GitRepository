package com.erbadagang.springboot.anti.reptile.antireptile.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * (Users)实体类
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:07
 */

@Data
@NoArgsConstructor
@Entity
@Table(name = "users", schema = "orders_1")
public class UsersEntity implements Serializable {

    private static final long serialVersionUID = -92907554413143227L;

    /**
     * 用户编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 账号
     */
    @Basic
    @Column(name = "username")
    private String username;

    /**
     * 密码
     */
    @Basic
    @Column(name = "password")
    private String password;

    /**
     * 创建时间
     */
    @Basic
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 删除标记，0未删除；1已删除。
     */
    @Basic
    @Column(name = "deleted")
    private Integer deleted;

}