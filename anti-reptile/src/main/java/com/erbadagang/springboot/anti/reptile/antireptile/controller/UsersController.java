package com.erbadagang.springboot.anti.reptile.antireptile.controller;

import cn.keking.anti_reptile.annotation.AntiReptile;
import com.erbadagang.springboot.anti.reptile.antireptile.entity.UsersEntity;
import com.erbadagang.springboot.anti.reptile.antireptile.service.IUsersService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * (Users)表控制层，通过配置反爬虫注解来启用反扒，如果触发规则会弹出页面输入验证码。
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:13
 */
@RestController
@RequestMapping("users")
public class UsersController {
    /**
     * 服务对象
     */
    @Resource
    private IUsersService usersService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @AntiReptile
    @GetMapping("selectOne")
    public Optional<UsersEntity> findById(Integer id) {
        return this.usersService.findById(id);
    }

}