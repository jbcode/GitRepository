package com.erbadagang.springboot.anti.reptile.antireptile.repository;

import com.erbadagang.springboot.anti.reptile.antireptile.entity.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * (Users)表数据库访问层
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:11
 */
public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {

}