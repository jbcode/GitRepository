package com.erbadagang.springboot.anti.reptile.antireptile.service;

import com.erbadagang.springboot.anti.reptile.antireptile.entity.UsersEntity;
import java.util.Optional;
/**
 * (Users)表服务接口
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:11
 */
public interface IUsersService {
    void save(UsersEntity usersEntity);
    
    Optional<UsersEntity> findById(Integer id);
}