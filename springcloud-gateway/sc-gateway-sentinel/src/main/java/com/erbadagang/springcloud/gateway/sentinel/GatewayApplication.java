package com.erbadagang.springcloud.gateway.sentinel;

import com.alibaba.cloud.sentinel.gateway.ConfigConstants;
import com.alibaba.csp.sentinel.config.SentinelConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 网关系统启动类，注意代码中设置应用类型为 Spring Cloud Gateway。
 * @ClassName: GatewayApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/31 11:09
 * @Copyright:
 */
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        // 【重点】设置应用类型为 Spring Cloud Gateway
        System.setProperty(SentinelConfig.APP_TYPE, ConfigConstants.APP_TYPE_SCG_GATEWAY);

        SpringApplication.run(GatewayApplication.class, args);
    }

}
