use `fescar`;
CREATE TABLE `sys_user`
(
    `user_id`         bigint  NOT NULL AUTO_INCREMENT,
    `username`        varchar(50)    NOT NULL unique ,
    `encode_password` varchar(50)       NOT NULL,
    `age`             int(3)           NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

insert into fescar.sys_user values (1,'guoxiuzhi','{noop}123456',18);