package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserWithSQLDeleteAnnotation;
import com.erbadagang.data.jpa.repository.UserRepositoryLogicalDel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

/**
 * @description 官方版本的逻辑删除实现。
 * @ClassName: UserRepositoryLogicalDelTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 20:28
 * @Copyright:
 */
@SpringBootTest(classes = JpaApplication.class)
public class UserRepositoryLogicalDelTest {
    @Autowired
    private UserRepositoryLogicalDel userRepository;

    @Test // 根据 ID 编号，删除一条记录，会变成逻辑删除的update语句。
        public void testDelete() {
        userRepository.deleteById(5);
    }

    @Test // 根据 ID 编号，查询一条记录
    public void testSelectById() {
        Optional<UserWithSQLDeleteAnnotation> userDO = userRepository.findById(1);
        System.out.println(userDO.get());
    }

}