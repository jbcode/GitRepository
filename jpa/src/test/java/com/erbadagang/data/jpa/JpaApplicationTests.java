package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserEntity;
import com.erbadagang.data.jpa.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @description JPA测试用例。
 * @ClassName: JpaApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/13 16:29
 * @Copyright:
 */
@SpringBootTest
@Slf4j
class JpaApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testFindAll() {
        //Jpa 分页查询
        Sort sort = Sort.by(Sort.Direction.DESC, "id"); //通过id进行倒序，id 是Bean 中的变量，不是数据库中的字段（*）
        int page = 0;
        Pageable pageable = PageRequest.of(page, 5, sort);  // page 从 0 开始 ，2是指每个page的大小，这个意思是id排序分页查询，每次查询2个数据

        Page<UserEntity> userPage = userRepository.findAll(pageable);
        userPage.forEach(System.out::println);
        log.info("总记录数：" + userPage.getTotalElements());
    }

    @Test
    void testFindByAgeLessThanPage() {
        //Jpa 分页查询
        Sort sort = Sort.by(Sort.Direction.DESC, "id"); //通过id进行倒序，id 是Bean 中的变量，不是数据库中的字段（*）
        int page = 0;
        Pageable pageable = PageRequest.of(page, 2, sort);  // page 从 0 开始 ，2是指每个page的大小，这个意思是id排序分页查询，每次查询2个数据

        Page<UserEntity> userPage = userRepository.findByAgeLessThan(20, pageable);
        userPage.getContent().forEach(System.out::println);
        log.info("总记录数：" + userPage.getTotalElements());
        log.info("总页数：" + userPage.getTotalPages());
    }

    @Test
    void testfindByAgeLessThanAndNameContains() {
        //Jpa 分页查询
        Sort sort = Sort.by(Sort.Direction.DESC, "id"); //通过id进行倒序，id 是Bean 中的变量，不是数据库中的字段（*）
        int page = 0;
        Pageable pageable = PageRequest.of(page, 2, sort);  // page 从 0 开始 ，2是指每个page的大小，这个意思是id排序分页查询，每次查询2个数据

        Page<UserEntity> userPage = userRepository.findByAgeLessThanAndNameContains(20, "Guo", pageable);
        userPage.getContent().forEach(System.out::println);
        log.info("总记录数：" + userPage.getTotalElements());
        log.info("总页数：" + userPage.getTotalPages());
    }
}
