package com.erbadagang.data.jpa.repository;

import com.erbadagang.data.jpa.base.BaseRepostitory;
import com.erbadagang.data.jpa.entity.UserBaseEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @description 多继承一个接口JpaSpecificationExecutor，用来动态拼装SQL。
 * @ClassName: UserSpecificationRepository
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/15 10:52
 * @Copyright:
 */
public interface UserSpecificationRepository extends BaseRepostitory<UserBaseEntity, Integer>, JpaSpecificationExecutor<UserBaseEntity> {

}