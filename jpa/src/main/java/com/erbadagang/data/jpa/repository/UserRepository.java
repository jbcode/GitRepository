package com.erbadagang.data.jpa.repository;

import com.erbadagang.data.jpa.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * (User)表数据库访问层
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-13 11:13:01
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    //查询<某年龄的所有用户数据并分页
    Page<UserEntity> findByAgeLessThan(Integer age, Pageable pageable);

    //查询<某年龄,并且姓名包括某字符串的所有用户数据并分页
    Page<UserEntity> findByAgeLessThanAndNameContains(Integer age, String name, Pageable pageable);
}