package com.erbadagang.data.jpa.repository;


import com.erbadagang.data.jpa.entity.UserDO;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * @description
 * @ClassName: UserRepository05
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 13:11
 * @Copyright:
 */
public interface UserRepository05 extends PagingAndSortingRepository<UserDO, Integer> {

    @Query("SELECT u FROM UserDO u WHERE u.username = ?1")
    UserDO findByUsername01(String username); // <1>

    @Query("SELECT u FROM UserDO u WHERE u.username = :username")
    UserDO findByUsername02(@Param("username") String username); // <2>

    @Query(value = "SELECT * FROM users u WHERE u.username = :username", nativeQuery = true)
    UserDO findByUsername03(@Param("username") String username); // <3>

    @Query("UPDATE UserDO  u SET u.username = :username WHERE u.id = :id")
    @Modifying
    int updateUsernameById(@Param("id") Integer id, @Param("username") String username); // <4>

}