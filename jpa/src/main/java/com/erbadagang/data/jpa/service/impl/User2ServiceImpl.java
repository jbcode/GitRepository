package com.erbadagang.data.jpa.service.impl;

import com.erbadagang.data.jpa.entity.UserEntity;
import com.erbadagang.data.jpa.repository.UserRepository;
import com.erbadagang.data.jpa.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * (User)表服务实现类
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-13 11:13:04
 */
@Service("userService2")
public class User2ServiceImpl implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    @Override
    public Optional<UserEntity> findById(Long id) {
        return userRepository.findById(id);
    }
}