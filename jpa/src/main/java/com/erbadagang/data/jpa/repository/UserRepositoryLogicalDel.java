package com.erbadagang.data.jpa.repository;

import com.erbadagang.data.jpa.entity.UserWithSQLDeleteAnnotation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description 官方版本的逻辑删除注解
 * 删除sql变成了update
 * @SQLDelete(sql = "update users set deleted = 1 where id = ?")
 * where条件带上了逻辑删除条件
 * @Where(clause = "deleted = 0")
 * 查看实体类注解：{@link com.erbadagang.data.jpa.entity.UserWithSQLDeleteAnnotation}
 * @ClassName: UserRepositoryLogiclDel
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 20:14
 * @Copyright:
 */
public interface UserRepositoryLogicalDel extends JpaRepository<UserWithSQLDeleteAnnotation, Integer> {

}