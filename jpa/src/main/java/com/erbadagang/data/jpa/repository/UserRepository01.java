package com.erbadagang.data.jpa.repository;

import com.erbadagang.data.jpa.entity.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description
 * @ClassName: UserRepository01
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 12:31
 * @Copyright:
 */
public interface UserRepository01 extends JpaRepository<UserDO, Integer> {

}