package com.erbadagang.mybatis.plus.tenant.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-12
 */
@RestController
@RequestMapping("/tenant")
public class TenantController {

}

