package com.erbadagang.mybatis.plus.tenant;

import com.erbadagang.mybatis.plus.tenant.config.ApiContext;
import com.erbadagang.mybatis.plus.tenant.entity.Tenant;
import com.erbadagang.mybatis.plus.tenant.mapper.TenantMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @description 多租户测试用例
 * @ClassName: MultiTanentApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/12 22:06
 * @Copyright:
 */
@SpringBootTest
class MultiTanentApplicationTests {

    @Autowired
    private ApiContext apiContext;

    @Autowired
    private TenantMapper tenantMapper;

    @Test
    public void before() {
        // 在上下文中设置当前服务商的ID
        apiContext.setCurrentTenantId(1L);
    }

    @Test
    public void select() {
        List<Tenant> tenants = tenantMapper.selectList(null);
        tenants.forEach(System.out::println);
    }

    @Test
    public void myCount() {
        Integer count = tenantMapper.myCount();
        System.out.println(count);
    }
}
